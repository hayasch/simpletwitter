<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>簡易Twitter</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
	<div class="header">
		<c:if test="${ empty loginUser }">
			<a href="login">ログイン</a>
			<a href="signup">登録する</a>
		</c:if>
		<c:if test="${ not empty loginUser }">
			<a href="./">ホーム</a>
			<a href="setting">設定</a>
			<a href="logout" onClick='return jumpConfirm(this);'>ログアウト</a>
		</c:if>
	</div>

	<c:if test="${ not empty loginUser }">
		<div class="profile">
			<div class="name">
				<h2>
					<c:out value="${loginUser.name}" />
				</h2>
			</div>
			<div class="account">
				@
				<c:out value="${loginUser.account}" />
			</div>
			<div class="description">
				<c:out value="${loginUser.description}" />
			</div>
		</div>
	</c:if>

	<div class="calender">
		<form action="index.jsp" method="get">
			日付：<input type="date" name="startDate" value="${startDate}" />～<input type="date" name="endDate" value="${endDate}" />
			<input type="submit" value="絞り込み"><br />
		</form>
	</div>

	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>

	<div class="form-area">
		<c:if test="${isShowMessageForm}">
			<form action="message" method="post">
				<textarea name="text" cols="100" rows="5" class="tweet-box" placeholder="いま、どうしてる？"></textarea>
				<br /> <input type="submit" value="つぶやく">（140文字まで）
			</form>
		</c:if>
	</div>

	<div class="messages">
		<c:forEach items="${messages}" var="message">
			<div class="message">

				<div class="account-name">
					<span class="account">
					<a href="./?user_id=<c:out value="${message.userId}" /> "><c:out value="${message.account}" />
					</a>
					</span>
					<span class="name"><c:out value="${message.name}" /></span>
				</div>

				<div class="text">
					<c:forEach items="${message.splitedText}" var="text">
					<pre><c:out value="${ text }" /></pre>
					</c:forEach>
				</div>

				<div class="date">
					<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
				</div>

				<div class = "editOrDelete">
					<c:if test="${loginUser.id==message.userId}">
						<form action="edit" method="get">
							<input type="submit" value="編集">
							<input type="hidden" name="messageId" value="${message.id}">
						</form>
						<form action="deleteMessage" method="post">
							<input type="submit" value="削除" id="delete" onClick="return Delete(this)">
							<input type="hidden" name="messageId" value="${message.id}">
						</form>
					</c:if>
				</div>

				<div class="comment">
					<c:if test="${not empty loginUser}">
						<form action="comment" method="post">
						<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
						<br /> <input type="submit" value="返信">
						<input type="hidden" name="messageId" value="${message.id}">
						</form>
					</c:if>
				</div>

				<div class="showComments">
					<c:forEach items="${comments}" var="comment">
					<c:if test="${comment.messageId==message.id}">

						<div class="account-name">
						<span class="account"><c:out value="${comment.account}" /></span>
						<span class="name"><c:out value="${comment.name}" /></span>
						</div>

						<div class="text">
							<c:forEach items="${comment.splitedText}" var="splitedText">
							<pre><c:out value="${ splitedText }" /></pre>
							</c:forEach>
							</div>

						<div class="date">
							<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
						</div>

					</c:if>
					</c:forEach>
				</div>
			</div>
		</c:forEach>
	</div>

	<div class="copyright">Copyright(c)HayashiKeita</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="./js/test.js"></script>
</body>
</html>
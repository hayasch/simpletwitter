package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/deleteMessage" })
public class DeleteMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//sessionを取得
		HttpSession session = request.getSession();
		//top.jspからidを取得
		String messageId = request.getParameter("messageId");
		int id = Integer.parseInt(messageId);

		Message message = new Message();
		message.setId(id);
		session.setAttribute("id", id);
		//MessageServiceのdeleteメソッドを呼び出し
		new MessageService().delete(message);
		//TopServletにお返し
		response.sendRedirect("./");
	}

}
